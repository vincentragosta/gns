<?php

use Backstage\SetDesign\Icon\IconView;
use Backstage\SetDesign\NavMenu\NavMenuView;
use Backstage\SetDesign\SocialIcons\SocialIcon;
use ChildTheme\Options\GlobalOptions;

?>
<header class="header-nav sticky-header header-nav--unloaded" data-gtm="Header">
    <div class="header-nav__container container">
        <div class="header-nav__logo-container">
            <a class="header-nav__logo-link" href="<?= home_url() ?>">
                <?php if ($header_image = GlobalOptions::headerBrandImage()): ?>
                    <?= $header_image->css_class('header-nav__logo')->height(32)->width(143); ?>
                <?php else: ?>
                    <strong><?php bloginfo('name'); ?></strong>
                <?php endif; ?>
            </a>
        </div>
        <div class="header-nav__content-container">
            <?php if (has_nav_menu('primary_navigation')): ?>
                <div class="header-nav__menu">
                    <?= NavMenuView::createResponsive('primary_navigation'); ?>
                </div>
            <?php endif; ?>
            <?php if (!empty($social_icons = GlobalOptions::socialIcons())): ?>
                <div class="header-nav__social">
                    <ul class="list list--inline">
                        <?php foreach($social_icons as $SocialIcon): /* @var SocialIcon $SocialIcon */ ?>
                            <li>
                                <a href="<?= $SocialIcon->getUrl(); ?>" class="link-icon--<?= $SocialIcon->getName(); ?>" target="<?= $SocialIcon->getTarget(); ?>">
                                    <?= new IconView(['icon_name' => $SocialIcon->getName()]); ?>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
        </div>
    </div>
</header>
