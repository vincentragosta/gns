<?php

// ==============================
// Composer autoloader if present
// ==============================
if (file_exists(__DIR__ . '/wp-content/vendor/autoload.php')) {
    define('USE_COMPOSER_AUTOLOADER', true);
    require_once __DIR__ . '/wp-content/vendor/autoload.php';
}

// ===================================================
// Load database info and local development parameters
// ===================================================
if (file_exists(__DIR__ . '/local-config.php')) {
    include(__DIR__ . '/local-config.php');
}

// ===================================================
// Initialize Situation defaults
// ===================================================
if (class_exists('\Situation\WPConfig')) {
    new \Situation\WPConfig(__DIR__);
}

// ================================================
// You almost certainly do not want to change these
// ================================================
define('DB_CHARSET', 'utf8mb4');
define('DB_COLLATE', '');

// ==============================================================
// Salts, for security
// Grab these from: https://api.wordpress.org/secret-key/1.1/salt
// ==============================================================
define('AUTH_KEY',         '#gEl-| 3=6RSs 4R_p(p&u)(4oNbth;6R^L?=tA|mIjxHz+~d!ss^4KyDzQTb|Jj');
define('SECURE_AUTH_KEY',  '5IBMuNw)-|LVO+Jyg*){wK(?`hF$z_v+&vx3fVCv}2-@W+uQjPxY8kUb1-oGM#6e');
define('LOGGED_IN_KEY',    'KI%tS*8GK1 +:G{06J%}@-hUD`D!`CC(yF:IELK2.e3?v|Exb/Kw(1C2E<E~KKH(');
define('NONCE_KEY',        '8_H;tkn[/xVXBZz=hP iYbjm8g-DKu`2y^@-MEJZ:,}ERzWEP-+I(?d70th=e}=#');
define('AUTH_SALT',        '*Wso3{2%ndxO:AGpIGH@3Df*bS,U&|~S3<<Iptp>1aDHc&j0Mv5)/_G_O5Gcg|pK');
define('SECURE_AUTH_SALT', 'u[|c:mFX+}Wi=`F$AhLtB<H*::!&_Jhkrl-7:]WP7m7B0@buVJt1V`EMS0m|Pf4+');
define('LOGGED_IN_SALT',   '/5y]_MdZlY+# Xg^:?0Gl|S|:}Ys5fAh}{FsVC-eIP lKXn%)v2A+[!jaFl;ysOf');
define('NONCE_SALT',       't4l]K]+=q_Diz+n#o6ynM6Mw*RYqYoU3k2X*FN]I8*d?di8N~8shVZ4|1q]=D-8m');


// ==============================================================
// Table prefix
// Change this if you have multiple installs in the same database
// Changing default to 'sit_' to enhance security
// ==============================================================
$table_prefix = 'sit_';

// ================================
// Language
// Leave blank for American English
// ================================
define('WPLANG', '');

// ===================
// Bootstrap WordPress
// ===================
if (! defined('ABSPATH')) {
    define('ABSPATH', dirname(__FILE__) . '/wp/');
}
require_once(ABSPATH . 'wp-settings.php');
