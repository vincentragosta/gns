<?php

use ChildTheme\Components\IconCard\IconCardView;
use ChildTheme\Options\GlobalOptions;
use ChildTheme\Service\Service;

global $wp_query, $post;
$Page = GlobalOptions::servicePage();
?>

<?php if (!empty($content = $Page->content(false))): ?>
    <?= $content; ?>
<?php endif; ?>
<?php if ($wp_query->have_posts()): ?>
    <section class="content-section">
        <div class="content-section__container container">
            <div class="content-row row">
                <?php while($wp_query->have_posts()): the_post(); ?>
                    <div class="content-column col-12 col-sm-6">
                        <div class="content-column__inner">
                            <?= IconCardView::createFromService(new Service($post)); ?>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </section>
<?php endif; ?>
