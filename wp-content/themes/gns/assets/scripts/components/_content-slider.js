addAction(INIT, function() {
    SetDesign.addFilter('content-slider/owl-settings', function(settings) {
        settings.nav = false;
        settings.dots = true;
        return settings;
    });
});
