<?php

namespace ChildTheme\Components\Card;

use Backstage\Util;
use Backstage\VcLibrary\Support\ComponentContainer;
use Backstage\View\Element;

/**
 * Class Card
 * @package ChildTheme\Components\Card
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class Card extends ComponentContainer
{
    const DEFAULT_TAG = 'div';
    const NAME = 'Card';
    const TAG = 'vc_card';
    const BASE_CLASS = 'card';

    protected $component_config = [
        'description' => 'Create a card.',
        'show_settings_on_create' => true,
        'is_container' => true,
        'content_element' => true,
        'js_view' => 'VcColumnView',
        'category' => 'Structure',
        'params' => []
    ];

    protected function createView(array $atts)
    {
        /* @var Element $Card */
        $attributes = [
            'class' => [static::BASE_CLASS]
        ];
        return Element::create(static::DEFAULT_TAG, $atts['content'])->attributes($attributes);
    }
}
