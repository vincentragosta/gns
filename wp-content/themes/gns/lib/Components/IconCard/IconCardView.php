<?php

namespace ChildTheme\Components\IconCard;

use Backstage\SetDesign\Icon\IconView;
use Backstage\View\Component;
use Backstage\View\Link;
use ChildTheme\Service\Service;

/**
 * Class IconCardView
 * @package ChildTheme\Components\IconCard
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 *
 * @property string $title
 * @property string content
 * @property array $link
 * @property string $icon
 */
class IconCardView extends Component
{
    protected $name = 'icon-card';
    protected static $default_properties = [
        'title' => '',
        'content' => '',
        'link' => '',
        'icon' => ''
    ];

    public function __construct(string $title, string $content, array $link = [], string $icon = '')
    {
        if (isset($link['title'])) {
            $link['title'] = $link['title'] . new IconView(['icon_name' => 'long-arrow']);
        }
        parent::__construct(compact('title', 'content', 'link', 'icon'));
        if (!empty($url = $this->link['url'])) {
            $this->title = new Link($url, $this->title);
        }
    }

    public static function createFromService(Service $Service)
    {
        return new static(
            $Service->title(),
            $Service->excerpt(150),
            [
                'title' => 'View Details',
                'url' => $Service->permalink()
            ],
            'cone'
        );
    }
}
