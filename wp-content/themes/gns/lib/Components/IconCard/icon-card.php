<?php
/**
 * Expected:
 * @var string $title
 * @var string $content
 * @var array $link
 * @var string $icon
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\SetDesign\Icon\IconView;
use Backstage\Util;
use Backstage\View\Link;

if (empty($title) || empty($icon)) {
    return;
}
?>

<div <?= Util::componentAttributes('icon-card', $class_modifiers, $element_attributes); ?>>
    <?php if ($icon): ?>
        <div class="icon-card__icon-container">
            <div class="icon-card__icon-container-inner">
                <?= new IconView(['icon_name' => $icon]); ?>
            </div>
        </div>
    <?php endif; ?>
    <div class="icon-card__content-container">
        <h3 class="icon-card__heading heading heading--default"><?= $title; ?></h3>
        <?php if ($content): ?>
            <div class="icon-card__content">
                <?= $content; ?>
            </div>
        <?php endif; ?>
        <?php if (isset($link['url'])): ?>
            <?= Link::createFromField($link)->class('icon-card__link'); ?>
        <?php endif; ?>
    </div>
</div>
