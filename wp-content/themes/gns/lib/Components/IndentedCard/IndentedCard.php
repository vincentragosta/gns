<?php

namespace ChildTheme\Components\IndentedCard;

use Backstage\Util;
use Backstage\VcLibrary\Support\Component;
use Backstage\View\Element;

/**
 * Class IndentedCard
 * @package Backstage\VcLibrary\Components
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class IndentedCard extends Component
{
    const NAME = 'Indented Card';
    const TAG = 'indented_card';
    const VIEW = IndentedCardView::class;

    protected $component_config = [
        'description' => 'Create an indented card.',
        'icon' => 'icon-wpb-atm',
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => [
            [
                'type' => 'attach_image',
                'heading' => 'Image',
                'param_name' => 'image_id',
                'description' => 'Set an image.',
                'group' => 'General'
            ],
            [
                'type' => 'textfield',
                'heading' => 'Heading',
                'param_name' => 'heading',
                'description' => 'Set the heading text',
                'group' => 'General',
                'admin_label' => true
            ],
            [
                'type' => 'textarea_html',
                'heading' => 'Content',
                'param_name' => 'content',
                'description' => 'Set the content',
                'group' => 'General',
            ]
        ]
    ];

    protected function createView(array $atts, $content = null)
    {
        $ViewClass = static::VIEW;
        return new $ViewClass(
            \WP_Image::get_by_attachment_id($atts['image_id']),
            $atts['heading'],
            $atts['content']
        );
    }
}
