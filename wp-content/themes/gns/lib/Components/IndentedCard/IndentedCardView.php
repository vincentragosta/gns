<?php

namespace ChildTheme\Components\IndentedCard;

use Backstage\View\Component;
use \WP_Image;

/**
 * Class IndentedCardView
 * @package ChildTheme\Components\IndentedCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 * @property WP_Image image
 * @property string $heading
 * @property string $content
 */
class IndentedCardView extends Component
{
    const IMAGE_SIZE = 768;

    protected $name = 'indented-card';
    protected static $default_properties = [
        'image' => false,
        'heading' => '',
        'content' => ''
    ];

    public function __construct(WP_Image $image, string $heading, string $content = '')
    {
        parent::__construct(compact('image', 'heading', 'content'));
        if ($image instanceof WP_Image) {
            $this->image->width(static::IMAGE_SIZE)->height(static::IMAGE_SIZE);
        }
    }
}
