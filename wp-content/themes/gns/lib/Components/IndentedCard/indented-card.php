<?php
/**
 * Expected:
 * @var WP_Image $image
 * @var string $heading
 * @var string $content
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\Util;

if (empty($heading)) {
    return;
}
?>

<div <?= Util::componentAttributes('indented-card', $class_modifiers, $element_attributes); ?>>
    <?php if ($image instanceof WP_Image): ?>
        <div class="indented-card__image">
            <?= $image; ?>
        </div>
    <?php endif; ?>
    <div class="indented-card__content-container">
        <div class="indented-card__content-container-inner">
            <h3 class="heading heading--default"><?= $heading; ?></h3>
            <?php if ($content): ?>
                <?= $content; ?>
            <?php endif; ?>
        </div>
    </div>
</div>
