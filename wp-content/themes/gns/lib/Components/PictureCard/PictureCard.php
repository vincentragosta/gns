<?php

namespace ChildTheme\Components\PictureCard;

use Backstage\VcLibrary\Support\Component;

/**
 * Class PictureCard
 * @package ChildTheme\Components\PictureCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class PictureCard extends Component
{
    const NAME = 'Picture Card';
    const TAG = 'picture_card';
    const VIEW = PictureCardView::class;

    protected $component_config = [
        'description' => 'Create a picture card.',
        'icon' => 'icon-wpb-atm',
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => [
            [
                'type' => 'attach_image',
                'heading' => 'Image',
                'param_name' => 'image_id',
                'description' => 'Set an image.',
                'group' => 'General'
            ],
            [
                'type' => 'textfield',
                'heading' => 'Heading',
                'param_name' => 'heading',
                'description' => 'Set the heading text',
                'group' => 'General',
                'admin_label' => true
            ],
        ]
    ];

    protected function createView(array $atts)
    {
        $ViewClass = static::VIEW;
        return new $ViewClass(\WP_Image::get_by_attachment_id($atts['image_id']), $atts['heading']);
    }
}
