<?php

namespace ChildTheme\Components\PictureCard;

use Backstage\SetDesign\Modal\ModalView;
use Backstage\View\Component;
use \WP_Image;

/**
 * Class PictureCardView
 * @package ChildTheme\Components\PictureCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 * @property WP_Image image
 * @property string $heading
 */
class PictureCardView extends Component
{
    const IMAGE_WIDTH = 768;
    const IMAGE_HEIGHT = 491;
    const MODAL_ID = 'picture-card-modal';

    protected $name = 'picture-card';
    protected static $default_properties = [
        'image' => false,
        'heading' => '',
    ];

    public function __construct(WP_Image $image, string $heading = '', array $link = [])
    {
        ModalView::load(static::MODAL_ID, 'box');
        parent::__construct(compact('image', 'heading', 'link'));
        if ($image instanceof WP_Image) {
            $this->image->width(static::IMAGE_WIDTH)->height(static::IMAGE_HEIGHT);
        }
    }
}
