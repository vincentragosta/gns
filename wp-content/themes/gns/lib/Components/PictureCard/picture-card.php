<?php
/**
 * Expected:
 * @var WP_Image $image
 * @var string $heading
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\Util;
use ChildTheme\Components\PictureCard\PictureCardView;

if (!$image instanceof WP_Image) {
    return;
}

$element_attributes['class'] = 'js-modal-builder';
$element_attributes['data-modal-target'] = '#' . PictureCardView::MODAL_ID;
?>

<div <?= Util::componentAttributes('picture-card', $class_modifiers, $element_attributes); ?>>
    <?= $image->css_class('picture-card__image'); ?>
    <div class="picture-card__overlay">
        <h3 class="picture-card__heading text--inverted"><?= $heading; ?></h3>
        <hr class="horizontal-rule horizontal-rule--center" />
    </div>
</div>
