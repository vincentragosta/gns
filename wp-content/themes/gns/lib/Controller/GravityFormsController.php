<?php

namespace ChildTheme\Controller;

use ChildTheme\Options\GlobalOptions;

/**
 * Class GravityFormsController
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class GravityFormsController
{
    const CONTACT_FORM_ID = 1;
    const EMAIL_MESSAGE = 'Dear Gio/Jeff,
    A new inquiry has been submitted:
    Name: %s
    Email: %s
    Message: %s';

    public function __construct()
    {
        add_action('gform_after_submission_' . static::CONTACT_FORM_ID, [$this, 'sendEmailOnSubmission'], 10, 2);
    }

    public function sendEmailOnSubmission($entry, $form)
    {
        if (empty($entry['id'])) {
            return;
        }
        wp_mail(
            GlobalOptions::contactEmail(),
            'GNS: Contact Inquiry',
            sprintf(static::EMAIL_MESSAGE, $entry['1.3'], $entry['2'], $entry['3'])
        );
    }
}
