<?php

namespace ChildTheme\Controller;

/**
 * Class VcLibraryController
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class VcLibraryController
{
    const TAGS = [
      'vc_section',
      'vc_row',
      'vc_column'
    ];

    protected static $additional_layout_options = [
        'Blue Tint' => 'tint-blue',
        'Red Tint' => 'tint-red',
        'Gray Tint' => 'tint-gray'
    ];

    protected static $additional_background_colors = [
        'Primary' => 'primary',
        'Bordered' => 'bordered'
    ];

    public function __construct()
    {
        foreach(static::TAGS as $tag) {
            add_filter('backstage/vc-library/'. str_replace('vc_', '', $tag) . '/additional-options', [$this, 'addLayoutOptions']);
            add_filter('backstage/vc-library/background-colors/' . $tag, [$this, 'addBackgroundColors']);
        }
    }

    public function addBackgroundColors($colors) {
        return array_merge($colors, static::$additional_background_colors);
    }

    public function addLayoutOptions($options)
    {
        return array_merge($options, static::$additional_layout_options);
    }
}
