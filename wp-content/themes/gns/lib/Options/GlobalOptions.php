<?php

namespace ChildTheme\Options;

use Backstage\Models\Page;
use Orchestrator\GlobalOptions as GlobalOptionsBase;
use \WP_Image;

/**
 * Class GlobalOptions
 * @package ChildTheme\Options
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 *
 * @method static WP_Image headerBrandImage()
 * @method static WP_Image stickyHeaderBrandImage()
 * @method static banner()
 * @method static socialIcons()
 * @method static contactAddress()
 * @method static contactEmail()
 * @method static contactPhone()
 * @method static additionalText()
 * @method static servicePage()
 */
class GlobalOptions extends GlobalOptionsBase
{
    protected $default_values = [
        'header__brand_image' => null,
        'sticky-header__brand_image' => null,
        'additional_text' => '',
        'banner' => [],
        'social_icons' => [],
        'contact_address' => '',
        'contact_email' => '',
        'contact_phone' => '',
        'service_page' => null
    ];

    public static function bannerText()
    {
        $banner = static::banner();
        return !empty($text = $banner['text']) ? $text : '';
    }

    public static function bannerCTA()
    {
        $banner = static::banner();
        return !empty($cta = $banner['cta']) ? $cta : [];
    }

    protected function getHeaderBrandImage()
    {
        $header_image = $this->get('header__brand_image');
        return \WP_Image::get_by_attachment_id($header_image);
    }

    protected function getStickyHeaderBrandImage()
    {
        $sticky_header_image = $this->get('sticky-header__brand_image');
        return \WP_Image::get_by_attachment_id($sticky_header_image);
    }

    protected function getServicePage()
    {
        $service_page_id = $this->get('service_page');
        return new Page($service_page_id);
    }
}
