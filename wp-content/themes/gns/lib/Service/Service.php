<?php

namespace ChildTheme\Service;

use Backstage\Models\PostBase;

/**
 * Class Service
 * @package ChildTheme\Service
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class Service extends PostBase
{
    const POST_TYPE = 'service';
}
