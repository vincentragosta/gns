<?php

namespace ChildTheme\Service;

use Backstage\Repositories\PostRepository;

/**
 * Class ServiceRepository
 * @package ChildTheme\Service
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class ServiceRepository extends PostRepository
{
    protected $model_class = Service::class;
}
