<?php

namespace ChildTheme;

use ChildTheme\Components\IndentedCard\IndentedCard;
use ChildTheme\Components\Card\Card;
use ChildTheme\Components\PictureCard\PictureCard;
use ChildTheme\Controller\GravityFormsController;
use ChildTheme\Controller\VcLibraryController;
use Orchestrator\Theme as ThemeBase;

/**
 * Class Theme
 *
 * Configure settings by overriding parent class constants
 *
 * @package Theme
 */
class Theme extends ThemeBase
{
    const REMOVE_DEFAULT_POST_TYPE = true;

    const PLATFORM_THEME_SUPPORT = [
        'set-design/nav-menu',
        'design-producer',
        'faq-producer',
        'video-producer',
        'preview-producer'
    ];

    const EXTENSIONS = [
        Card::class,
        VcLibraryController::class,
        GravityFormsController::class,
        IndentedCard::class,
        PictureCard::class
    ];

    /**
     * Add theme-specific hooks
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Add theme-specific style and script enqueues
     */
    public function assets()
    {
        parent::assets();
    }
}
