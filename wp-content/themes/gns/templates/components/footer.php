<?php

use Backstage\SetDesign\Icon\IconView;
use Backstage\SetDesign\Modal\ModalView;
use Backstage\SetDesign\SocialIcons\SocialIcon;
use Backstage\View\Link;
use ChildTheme\Options\GlobalOptions;
use ChildTheme\Service\Service;
use ChildTheme\Service\ServiceRepository;

$ServiceRepository = new ServiceRepository();
?>

<?php if (!empty($banner_text = GlobalOptions::bannerText())): ?>
    <div class="footer-banner">
        <div class="footer-banner__container container">
            <div class="footer-banner__row row">
                <div class="col-12 col-sm-9">
                    <p class="footer-banner__text text--inverted"><?= $banner_text; ?></p>
                </div>
                <?php if (!empty($banner_cta = GlobalOptions::bannerCTA())): ?>
                    <div class="col-12 col-sm-3">
                        <?= Link::createFromField($banner_cta)->class('button button--inverted'); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<footer class="footer-nav">
    <div class="footer-nav__container container">
        <div class="footer-nav__row row">
            <div class="col-12 col-sm-4">
                <h2 class="heading heading--default heading--inverted">Our Services</h2>
                <hr class="horizontal-rule" />
                <?php if (!empty($services = $ServiceRepository->findAll())): ?>
                    <ul class="list--unstyled">
                        <?php foreach($services as $Service): ?>
                            <?php /* @var Service $Service */ ?>
                            <li><?= new Link($Service->permalink(), $Service->title()); ?></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </div>
            <div class="col-12 col-sm-4">
                <h2 class="heading heading--default heading--inverted">Location</h2>
                <hr class="horizontal-rule" />
                <ul class="list--unstyled">
                    <?php if ($address = GlobalOptions::contactAddress()): ?>
                        <li><?= $address; ?></li>
                    <?php endif; ?>
                    <?php if ($phone = GlobalOptions::contactPhone()): ?>
                        <li>T: <a href="<?= sprintf('tel:%s', $phone); ?>"><?= $phone; ?></a></li>
                    <?php endif; ?>
                    <?php if ($email = GlobalOptions::contactEmail()): ?>
                        <li>E: <a href="<?= sprintf('mailto:%s', $email); ?>"><?= $email; ?></a></li>
                    <?php endif; ?>
                </ul>
            </div>
            <div class="col-12 col-sm-4">
                <h2 class="heading heading--default heading--inverted">Affiliates</h2>
                <hr class="horizontal-rule" />
                <p><a href="https://www.inline-corp.com/" target="_blank">Inline Electrical Contractors, Inc.</a></p>
            </div>
            <div class="col-12 text--center footer-nav__social">
                <?php if (!empty($social_icons = GlobalOptions::socialIcons())): ?>
                    <ul class="list list--inline">
                        <?php foreach($social_icons as $SocialIcon): /* @var SocialIcon $SocialIcon */ ?>
                            <li>
                                <a href="<?= $SocialIcon->getUrl(); ?>" class="link-icon--<?= $SocialIcon->getName(); ?>" target="<?= $SocialIcon->getTarget(); ?>">
                                    <?= new IconView(['icon_name' => $SocialIcon->getName(), 'style' => 'circle']); ?>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </div>
            <div class="col-12 text--center">
                &copy; Copyright <?= (new DateTime('now'))->format('Y'); ?> - By <a href="https://vincentragosta.io/" target="_blank">Vincent Ragosta</a>
            </div>
        </div>
    </div>
</footer>

<?= ModalView::load('newsletter','box', do_shortcode(apply_filters('orchestrator/newsletter-shortcode', '[gravityform id="1" title=true description=true ajax=true]'))); ?>
<?= ModalView::unloadAll(); ?>
